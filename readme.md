PHP SDK
=======

- version: 1.3.0
- compatible with: API version 1.3


Installation:
-------------

1. Clone this repository.

2. Create a bootstrap script containing:

        require 'vcard/matchmovepay.php';
        MatchMovePay::init();

3. Define the connection details like, the target host, consumer_key, consumer_secret. You can also define the SSL certificate location when required. You can follow this [reference](http://unitstep.net/blog/2009/05/05/using-curl-in-php-to-access-https-ssltls-protected-sites)

    The host for connecting to testing is, `https://beta-api.mmvpay.com/<product>/v1` while `https://api.mmvpay.com/<product>/v1` should be used for production.
    
        $conn = new MatchMovePay\VCard\Connection('https://api.mmvpay.com/sg/v1', $key, $secret);
    or,
    
        $conn = new MatchMovePay\VCard\Connection('https://api.mmvpay.com/sg/v1', $key, $secret, $path_to_certificate);

4. From here on, you can either,

    * __Consume with a specific user__. To that, you need to request for the user's authentication credentials.

        $conn->authenticate($user, $password);
        $response = $conn->consume('GET', 'users');

    * __Consume anonymously__.

        $response = $conn->consume('POST', 'users', ['email' => 'some@one.com', ... ]);


Extra
-----

This SDK has a session handling feature that is being used in storing _oauth tokens_. This feature can be overwritten by:

_bootstrap.php_
    
    <?php
    require 'vcard/matchmovepay.php';
    MatchMovePay::init();

    // overwrite the default autoloading
    require 'path/to/my/new/cache.php';

    $conn = new MatchMovePay\VCard\Connection('https://api.mmvpay.com/sg/v1', $key, $secret);
    
_path/to/my/new/cache.php_

    <?php
    namespace MatchMovePay\Helper;

    class Cache implements \MatchMovePay\Helper\Abs\Cache {
        
        public static function set($key, $value, $expire = 900) {
            // This is where it stores the value.
            // Expiry must be 900 secs max.
        }
    
        public static function get($key) {
            // When the key is found, return here.
        }
    }

Issues
------

[Any inquiries, issues or feedbacks?](/matchmove/api-vcard-sdk-php/issues)