<?php
namespace MatchMovePay\VCard;

use MatchMovePay\Helper\Exception;
use MatchMovePay\Helper\Curl;
use MatchMovePay\Helper\OAuth;
use MatchMovePay\Helper\Cache;

class Connection {
    
    const OK = 200;
    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';
    const METHOD_PUT = 'PUT';
    const METHOD_DELETE = 'DELETE';
    
    protected $key    = null;
    protected $secret = null;
    protected $host   = null;
    protected $request          = null;
    protected $signature_method = 'HMAC-SHA1';
    protected $oauth_version    = '1.0';
    
    private $access = null;
        
    public function __construct($host, $key, $secret, $ssl_certificate = null) {
        $this->request = new Curl($host, $ssl_certificate);
        $this->host   = $host;
        $this->key    = $key;
        $this->secret = $secret;
    }
    
    public function get_default_query(array $query = null) {
        $query = array_merge([
            'oauth_consumer_key'     => $this->key,
            'oauth_nonce'            => OAuth::get_nonce(),
            'oauth_signature_method' => $this->signature_method,
            'oauth_timestamp'        => time(),
	        'oauth_version'          => $this->oauth_version
        ], $query);
        
        ksort($query);
        
        return $query;
    }
                    
    public function get_signature($secret, $method, $url, array $query = []) {
        $contents = implode('&', [
            OAuth::urlencode($method),
            OAuth::urlencode($this->host . '/' . $url),
            OAuth::urlencode(http_build_query($query, '', '&', PHP_QUERY_RFC3986))
        ]);
        
        if (is_array($secret)) {
            $secret = implode('&', $secret);
        }
        
        return Algo::factory($this->signature_method, $contents, $secret);
    }

    public function authenticate($user, $password = null) {
        $cache = hash('sha1', get_class($this) . __FUNCTION__ . $user . $this->host);
            
        $tokens = Cache::get($cache);
        if (!empty($tokens)) {
            return $this->access = (object) $tokens;
        }

        if (empty($password)) {
            return false;
        }
        
        $this->access = $this->get_access($this->get_request($user, $password));
        
        Cache::set($cache, $this->access);
        
        return $this->access;
    }
    
    
    public function consume($method, $api, array $params = []) {
        $params = !empty($this->access)
            ? $this->prepare_consume_oauth($method, $api, $params)
            : $this->prepare_consume_public($method, $api, $params);
        
        $response = $this->request->factory($method, $api, $params);

        if (Connection::OK != $response->status) {
            throw new Exception($response->head['message'] . $response->body, array(), $response->status);
        }
        
        return $response->body;
    }
    
    protected function prepare_consume_oauth($method, $api, array $query = []) {
        
        if (empty($this->access->oauth_token)) {
            throw new Exception('Access token\'s response is invalid or empty');
        }
        
        $query = $this->get_default_query(array_merge($query, [
            'oauth_token' => $this->access->oauth_token
        ]));
        
        $query['oauth_signature'] = $this->get_signature(
            [$this->secret, $this->access->oauth_token_secret], strtoupper($method), $api, $query)->encode64();
        
        return $query;
    }
    
    protected function prepare_consume_public($method, $api, array $query = []) {
        $query = $this->get_default_query([
            '_d' => urldecode(http_build_query($query))
        ]);
        
        $query['oauth_signature'] = $this->get_signature(
            $this->secret, strtoupper($method), $api, $query)->encode64();
        
        $query['_d'] = OAuth::encrypt($this->secret, $query['oauth_signature'], $query['_d']);
        
        return $query;
    }

    private function get_request($user, $password) {
        $api = 'oauth/request/token';
        $query = $this->get_default_query([
            'oauth_user_name'        => $user,
            'oauth_user_password'    => $password
        ]);
        
        $query['oauth_signature'] = $this->get_signature(
            $this->secret, 'POST', $api, $query)->encode64();
        
        $query['oauth_user_name'] =
            OAuth::encrypt($this->secret, $query['oauth_signature'], $user);
        $query['oauth_user_password'] =
            OAuth::encrypt($this->secret, $query['oauth_signature'], $password);
            
        $response = $this->request->post($api, $query);
        
        if (Connection::OK != $response->status) {
            throw new Exception($response->head['message'], array(), $response->status);
        }
        
        return json_decode($response->body);
    }
    
    private function get_access($request) {
        $api = 'oauth/access/token';
        
        if (empty($request->oauth_token)) {
            throw new Exception('Request token\'s response is invalid or empty');
        }
        $query = $this->get_default_query([
            'oauth_token' => $request->oauth_token
        ]);
        
        $query['oauth_signature'] = $this->get_signature(
            [$this->secret, $request->oauth_token_secret], 'POST', $api, $query)->encode64();
            
        $response = $this->request->post($api, $query);
        
        if (Connection::OK != $response->status) {
            throw new Exception($response->head['message'], array(), $response->status);
        }
        
        return json_decode($response->body);
    }
}